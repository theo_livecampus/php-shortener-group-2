<?php

namespace App\Controller;

use App\Entity\Url;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/home', name: 'index')]
    public function index(ManagerRegistry $doctrine, Request $request): Response
    {
        $urls = $doctrine->getRepository(Url::class)->findAll();
        $entityManager = $doctrine->getManager();

        $url = new Url();

        $form = $this->createFormBuilder($url)->add('url', TextType::class)->add('submit', SubmitType::class)->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $url = $form->getData();
            $url->setCreator($this->getUser());
            $url->setShort(bin2hex(random_bytes(5)));
            $url->setNbclick(0);

            $entityManager->persist($url);
            $entityManager->flush();

            return $this->redirectToRoute("index");
        }

        return $this->render('index/index.html.twig', [
            'urls' => $urls,
            'form' => $form->createView()
        ]);
    }

    #[Route("/s/{short}", name:"app_short")]
    public function shortener(ManagerRegistry $doctrine, string $short)
    {
        $urls = $doctrine->getRepository(Url::class)->findAll();
        $entityManager = $doctrine->getManager();

        foreach($urls as $url) {
            if($url->getShort() == $short) {
                $url->setNbclick($url->getNbclick() +1);
                $entityManager->persist($url);
                $entityManager->flush();

                return $this->redirect($url->getUrl());
            }
        }
        return $this->redirectToRoute("index");
    }

    #[Route("/", name: 'redirect_index')]
    public function redirectindex(): RedirectResponse
    {
        return $this->redirectToRoute('index');
    }
}
