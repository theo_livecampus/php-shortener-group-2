<div align="center">
    <img src="https://a3u3t4j5.rocketcdn.me/wp-content/uploads/2020/09/logo_transparent_background.png" height="100px">
    <h1>LiveCampus - URL shortener (groupe 2)</h1>
</div>

<div align="center">

[![Symfony](https://img.shields.io/badge/symfony-v6.0.4-FFFFFF.svg?style=for-the-badge&logo=symfony)](https://symfony.com/)

</div>

---

## 📝 Table des matières

- [📝 Table des matières](#-table-des-matières)
- [📚 Énoncé du projet](#rules)
- [⚙️ Installation du projet](#install)
- [👨‍👦‍👦 Liste des participants](#credits)

## 📚 Énoncé du projet <a name = "rules"></a>

L'objectif est de réaliser un raccoourcisseur de liens avec PHP, en répondant aux demandes suivantes :

- Pouvoir se créer un compte
- Pouvoir se connecter à son compte
- Pouvoir se déconnecter de son compte
- Pouvoir lister ses URLs raccoourcies
- Pouvoir raccourcir une URL
- Pouvoir cliquer sur une URL raccourcie et être redirigé vers le lien initial

De plus nous avons abordé le point bonus suivant :

- Afficher le nombre de clics sur un lien raccourci

## ⚙️ Installation du projet <a name = "install"></a>

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png" height="15px" align="center"> Le projet nécessite d'abord d'installer PHP en version 8.0.2 ou supérieure, avec les extensions suivantes : `Ctype`, `iconv`, `PCRE`, `Session`, `SimpleXML`, et `Tokenizer`

<img src="https://cdn.freebiesupply.com/logos/large/2x/composer-logo-png-transparent.png" height="20px" align="center"> Ensuite il est également nécessaire d'installer composer.

<img src="https://symfony.com/logos/symfony_black_03.png" height="20px" align="center"> De manière optionnelle, il est aussi recommandé d'installer <a href="https://symfony.com/download">Symfony CLI</a> pour obtenir tout les outils disponibles

<img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="20px" align="center"> Pour obtenir le projet, il faut le cloner en local:

`git clone git@gitlab.com:livecampus/php-shortener-group-2.git php-shortener && cd php-shortener`

Puis installer ses dépendances:

`composer install`

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png" height="15px" align="center">
<img src="https://www.docker.com/sites/default/files/d8/2019-07/vertical-logo-monochromatic.png" height="15px" align="center"> Pour la base de données, notre projet utilise docker, qui fourni un container qui permet de mettre en place très rapidement le projet, il est également possible de lancer une base de donnée non dockerisée :

- Avec docker, il suffit de lancer le container avec `docker-compose up`,
- Sans docker, il faut changer le contenu du fichier `.env`, notemment les variables : `DATABASE_URL`, `POSTGRES_USER` et `POSTGRES_PASSWORD`

Une fois la base de données lancée, il faut penser à installer le schema, pour ceci, symfony procure la commande suivante:

`symfony console d:m:m`

Cette commande execute tous les fichiers de migrations qui n'ont pas encore été lancés dans la base de données. Ainsi la structure sera a jour et prete à recevoir des données.

Une fois la base de données mise en place, il suffit de lancer l'application:

- En arrière plan: `symfony serve -d`
- En premier plan: `symfony serve`

🎉 Puis de se rendre à l'addresse <a href="http://localhost:8000">http://localhost:8000</a> 🎉

## 👨‍👦‍👦 Liste des participants <a name = "credits"></a>

[![ThéoPosty](https://img.shields.io/badge/theo_posty-visit-blue.svg?style=for-the-badge&logo=data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAAZABkDASIAAhEBAxEB/8QAGQAAAgMBAAAAAAAAAAAAAAAABQYAAgQI/8QAKxAAAQMDAwIDCQAAAAAAAAAAAQIDBAAFERIhMQZREzKBFCNBQmFxkaGx/8QAGAEAAwEBAAAAAAAAAAAAAAAAAQIDBAf/xAAeEQACAwACAwEAAAAAAAAAAAABAgADEQQhE0GBkf/aAAwDAQACEQMRAD8A4G6fsKr7JLZfRFaSQFPOAkAk4A237n0NbD05bPbA2OpoBY05L3gv89tPh5rfY06E2cxd8FbyBxrlJ8qVdsDGO9alNMwL/aHZMWJIcTbH332kx20tqdSJBAKAnScaUjj5a63XxiUDDPv7GRNEXuoem1WLwVoktTY7oBS8z5dwFD7ZBB335yARQX0/VOt0fVOM0zEobzCaVIS0gIQ06kBLYSkbA4AGkYHNJeaycirxtkmRhhewXtFpcUl9kvsEhwJB3QscKGfWisXqu2RZkN4W991cdhxguvPBRUFlwk6QAM+8I5x9M0qVKVbmChT6gDZ1Ct2uzUpgMRg6ELcLzy3iNTiz3x8AP7Qn81O1XqT2NYdMBO9z/9k=)](https://github.com/theo-coder)
[![IliyanaIlieva](https://img.shields.io/badge/iliyana_ilieva-visit-purple.svg?style=for-the-badge&logo=data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gIoSUNDX1BST0ZJTEUAAQEAAAIYAAAAAAQwAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAAHRyWFlaAAABZAAAABRnWFlaAAABeAAAABRiWFlaAAABjAAAABRyVFJDAAABoAAAAChnVFJDAAABoAAAAChiVFJDAAABoAAAACh3dHB0AAAByAAAABRjcHJ0AAAB3AAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAFgAAAAcAHMAUgBHAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z3BhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABYWVogAAAAAAAA9tYAAQAAAADTLW1sdWMAAAAAAAAAAQAAAAxlblVTAAAAIAAAABwARwBvAG8AZwBsAGUAIABJAG4AYwAuACAAMgAwADEANv/bAEMAAwICAwICAwMDAwQDAwQFCAUFBAQFCgcHBggMCgwMCwoLCw0OEhANDhEOCwsQFhARExQVFRUMDxcYFhQYEhQVFP/bAEMBAwQEBQQFCQUFCRQNCw0UFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFP/AABEIAAoACgMBIgACEQEDEQH/xAAWAAEBAQAAAAAAAAAAAAAAAAAHBgn/xAAkEAABBAEDAwUAAAAAAAAAAAABAgMEBREABgcIITITMUFCUf/EABQBAQAAAAAAAAAAAAAAAAAAAAb/xAAbEQACAgMBAAAAAAAAAAAAAAABAgMRAARREv/aAAwDAQACEQMRAD8AoWeZbji9LW8ru8uL2Eptpm0rZj49NK3FZUuE35JS2peAHTkpHl9tIUbqd2/OjNSWrCpDbyA4kOWSEqAIyMgjIPf2OsyuM7yxn7IvIkmwlSIrBYS0w68pSGwXWwQlJOB2AHb80aZI+dHxrPKWUPVHgxRJtRxKjmO/Q6c//9k=)](https://gitlab.com/isilieva)
[![ClementFretay](https://img.shields.io/badge/clement_fretay-visit-green.svg?style=for-the-badge)](https://gitlab.com/cfretay)
[![MickaelLebas](https://img.shields.io/badge/mickael_lebas-visit-red.svg?style=for-the-badge&logo=data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gIoSUNDX1BST0ZJTEUAAQEAAAIYAAAAAAQwAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAAHRyWFlaAAABZAAAABRnWFlaAAABeAAAABRiWFlaAAABjAAAABRyVFJDAAABoAAAAChnVFJDAAABoAAAAChiVFJDAAABoAAAACh3dHB0AAAByAAAABRjcHJ0AAAB3AAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAFgAAAAcAHMAUgBHAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z3BhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABYWVogAAAAAAAA9tYAAQAAAADTLW1sdWMAAAAAAAAAAQAAAAxlblVTAAAAIAAAABwARwBvAG8AZwBsAGUAIABJAG4AYwAuACAAMgAwADEANv/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAAoACgMBEQACEQEDEQH/xAAWAAEBAQAAAAAAAAAAAAAAAAAHCAn/xAAiEAACAwEAAgICAwAAAAAAAAACBQEDBAYHCBEUABMJFRb/xAAZAQABBQAAAAAAAAAAAAAAAAAHAgMEBQb/xAAiEQADAQEAAQQDAQEAAAAAAAABAgMEBREABhIhBxMiFjH/2gAMAwEAAhEDEQA/AHj2j9wfMWtmsvp5rkmD3itVBu+Bx9lqw6OmWrVXTYu9xWPGuHHkLKn1V879C9rzOQOhc71wIR2pc9z2ydo6jautw9rUrz8x5fU3GD7Wz5qJo/TLPHTBS/7dUnS0nZ1Oeej4/qr9j5bvLy+fi0+5vcPN4c+97ly9zlZeTlx5m0NHSHre+nnxaEQZPi0SujGIrqEkZ80VIb0c5Pb5U3yZW3+Z7XJ/aZ6GP1NvjpDGzL92odP19cU0X1Rpo/b+q+Kr7q4tE4C2wfg5uXvFGZHF1dGKOrlw6upIZWH14YEEMPA8EH69HDl/k32/fmc65/3TG2HJUsvtvl2VjTPN/ktl6gWqn5eRRQFcf0AAfHqCe9bth9hf5BQFmxEEHo75TbIgjbpgUrWxSlrNmpGLfhcwOvdtA9uOKdJBs1DNkjotg3XlNI9nQk0TQu2ardUVbKqcz5IoqAHCo1KMoDeFNHIALt5HftuaHjbgUQhPx375qoKgha07j53ooI/mj55Sg7jwzRlORJRFUYOdB5Z8qVvnddfkvyBXXW3ZAAB2XRiAAOy4RABFlAiIjEQIxERERERERH4nXSh1aSXck6LEksxJJoxJJJ8kk/ZJ/wC+ghk7naz5c0IdjqRhHPGUYy6GucpSnNUnKU0sqTnNFCIiAKqgKoAAHr//2Q==)](https://gitlab.com/mickael.lebas)
